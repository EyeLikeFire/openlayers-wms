# Working with Geoserver WMS APIs

# Setup

Make sure to install the necessary packages with NPM.

```bash
npm install
```

To run the project you can run the `start:dev` NPM script:

```bash
npm run start:dev
```

This will launch your application on port 3002.

# Creating a new Layer with WMS source

We will have to create a new Layer that has a WMS source in `index.js`

```javascript
const nitrogenCornManureLayer = new TileLayer({
  visible: true,
  source: new TileWMS({
    url: 'https://sedac.ciesin.columbia.edu/geoserver/wms',
    projection: 'EPSG:3857',
    params: {
      layers: [
        'ferman-v1:ferman-v1-pest-chemgrids_app-rate-glyphosate-corn-high-est-2015'
      ]
    }
  })
});
```

Don't forget to import the TileWMS class from the OpenLayers module!

The source property of our TileLayer is the most interesting aspect of this code.

For our new TileWMS source object we must include:
 
- a URL to the geoserver that holds our data
- a map projection
- request parameters in this case it's simply the Layers we are requesting from this geoserver

# Adding the layer to our map

After creating the Layer with the WMS source we need to add it to our map:

```javascript
map.addLayer(nitrogenCornManureLayer)
```

After we have created the layer we need to add it to our map in order for it to be seen.

# Solution

You can see the entire solution by checking out the `solution` branch on this repository.

# More WMS Layers!!!

You can find more publicly hosted WMS layers from SEDAC on their [website hosted by columbia.edu](https://sedac.ciesin.columbia.edu/data/sets/browse?facets=data-type:map%20service)

Take a stab at adding a new Layer from a different data set from the large collection found at the link above.